
e3-subs
======
ESS Site-specific EPICS module : subs

This module exists to test an error that exists while inflating .db files from .substitution files.

If you run the following:

```bash
make install
make install
```

Then on the first pass through, you run into the "Installing module template files" only a single time.
However, on the second pass through you run into it multiple times, once for each entity in the variable
`TEMPLATES` (For times, in this case). The problem appears to be that `make db` creates a *new* `.db`
file from the `.substitution` file, which is *newer* than all of the other files in `TEMPLATES`. As such,
each of those targets must be rebuilt.